#3) Escreva um script em Python que receba dois números e que seja realizado as seguintes
#operações:
#• soma dos dois números
#• diferença dos dois números
#O resultado deverá ser apresentado conforme a seguir - no exemplo foram digitados os números
#4 e 2:
#------------------------------
#Soma: 4 + 2 = 6
#Diferença: 4 - 2 = 2
n1 = input('Digite um numero:')
n2 = input('Digite outro numero:')
print('------------------------------')
print(f'Soma: {n1} + {n2} = {int(n1) + int(n2)}')
print(f'Diferença: {n1} - {n2} = {int(n1) - int(n2)}')
